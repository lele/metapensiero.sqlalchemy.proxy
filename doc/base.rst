.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- Base functionalities
.. :Created:   ven 30 dic 2016 18:53:53 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016, 2017 Lele Gaifax
..

Abstract base class
===================

.. automodule:: metapensiero.sqlalchemy.proxy.base

.. autoclass:: ProxiedBase
   :members:
   :special-members: __call__
