.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- Sphinx master file
.. :Created:   ven 30 dic 2016 18:39:18 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016, 2017 Lele Gaifax
..

===============================
 metapensiero.sqlalchemy.proxy
===============================

Expose SQLAlchemy's queries and their metadata to a webservice
==============================================================

This package contains a few utilities to make it easier applying some filtering to a stock
query and obtaining the resultset in various formats.

.. toctree::
   :maxdepth: 2

   api
   usage
   changes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
