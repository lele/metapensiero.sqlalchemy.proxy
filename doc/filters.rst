.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- Filtering related stuff
.. :Created:   sab 11 feb 2017 11:22:41 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Lele Gaifax
..

Filtering functions
===================

.. automodule:: metapensiero.sqlalchemy.proxy.filters
   :members:
