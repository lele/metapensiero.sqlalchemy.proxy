.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- JSON documentation
.. :Created:   sab 22 lug 2017 17:34:00 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Lele Gaifax
..

JSON related functions
======================

.. automodule:: metapensiero.sqlalchemy.proxy.json
   :members:
