.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- Core functionalities
.. :Created:   ven 30 dic 2016 18:56:26 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016, 2017 Lele Gaifax
..

Core statements
===============

.. automodule:: metapensiero.sqlalchemy.proxy.core

.. autoclass:: ProxiedQuery
   :members:
   :special-members: __init__
