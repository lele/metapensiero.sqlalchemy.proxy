.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- API documentation
.. :Created:   ven 30 dic 2016 18:50:34 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016, 2017 Lele Gaifax
..

=============
 Exposed API
=============

.. toctree::
   :maxdepth: 2

   base
   core
   orm
   json
   filters
   sorters
   types
   utils
