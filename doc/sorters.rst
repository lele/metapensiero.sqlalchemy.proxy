.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.proxy -- Sorting related stuff
.. :Created:   dom 12 feb 2017 16:20:11 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Lele Gaifax
..

Sorting functions
=================

.. automodule:: metapensiero.sqlalchemy.proxy.sorters
   :members:
